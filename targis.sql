-- MySQL dump 10.13  Distrib 5.6.24, for Win64 (x86_64)
--
-- Host: localhost    Database: gis
-- ------------------------------------------------------
-- Server version	5.6.25-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `csv_dump`
--

DROP TABLE IF EXISTS `csv_dump`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `csv_dump` (
  `column0pk` int(11) NOT NULL AUTO_INCREMENT,
  `csv_dumpcol1` varchar(100) DEFAULT NULL,
  `csv_dumpcol2` varchar(100) DEFAULT NULL,
  `csv_dumpcol3` varchar(100) DEFAULT NULL,
  `csv_dumpcol4` varchar(100) DEFAULT NULL,
  `csv_dumpcol5` varchar(100) DEFAULT NULL,
  `csv_dumpcol6` varchar(100) DEFAULT NULL,
  `csv_dumpcol7` varchar(100) DEFAULT NULL,
  `csv_dumpcol8` varchar(100) DEFAULT NULL,
  `csv_dumpcol9` varchar(100) DEFAULT NULL,
  `csv_dumpcol10` varchar(100) DEFAULT NULL,
  `csv_dumpcol11` varchar(100) DEFAULT NULL,
  `csv_dumpcol12` varchar(100) DEFAULT NULL,
  `csv_dumpcol13` varchar(100) DEFAULT NULL,
  `csv_dumpcol14` varchar(100) DEFAULT NULL,
  `csv_dumpcol15` varchar(100) DEFAULT NULL,
  `csv_dumpcol16` varchar(100) DEFAULT NULL,
  `csv_dumpcol17` varchar(100) DEFAULT NULL,
  `csv_dumpcol18` varchar(100) DEFAULT NULL,
  `csv_dumpcol19` varchar(100) DEFAULT NULL,
  `csv_dumpcol20` varchar(100) DEFAULT NULL,
  `csv_dumpcol21` varchar(100) DEFAULT NULL,
  `csv_dumpcol22` varchar(100) DEFAULT NULL,
  `csv_dumpcol23` varchar(100) DEFAULT NULL,
  `csv_dumpcol24` varchar(100) DEFAULT NULL,
  `csv_dumpcol25` varchar(100) DEFAULT NULL,
  `csv_dumpcol26` varchar(100) DEFAULT NULL,
  `csv_dumpcol27` varchar(100) DEFAULT NULL,
  `csv_dumpcol28` varchar(100) DEFAULT NULL,
  `csv_dumpcol29` varchar(100) DEFAULT NULL,
  `csv_dumpcol30` varchar(100) DEFAULT NULL,
  `csv_dumpcol31` varchar(100) DEFAULT NULL,
  `csv_dumpcol32` varchar(100) DEFAULT NULL,
  `csv_dumpcol33` varchar(100) DEFAULT NULL,
  `csv_dumpcol34` varchar(100) DEFAULT NULL,
  `csv_dumpcol35` varchar(100) DEFAULT NULL,
  `csv_dumpcol36` varchar(100) DEFAULT NULL,
  `csv_dumpcol37` varchar(100) DEFAULT NULL,
  `csv_dumpcol38` varchar(100) DEFAULT NULL,
  `csv_dumpcol39` varchar(100) DEFAULT NULL,
  `csv_dumpcol40` varchar(100) DEFAULT NULL,
  `csv_dumpcol41` varchar(100) DEFAULT NULL,
  `csv_dumpcol42` varchar(100) DEFAULT NULL,
  `csv_dumpcol43` varchar(100) DEFAULT NULL,
  `csv_dumpcol44` varchar(100) DEFAULT NULL,
  `csv_dumpcol45` varchar(100) DEFAULT NULL,
  `csv_dumpcol46` varchar(100) DEFAULT NULL,
  `csv_dumpcol47` varchar(100) DEFAULT NULL,
  `csv_dumpcol48` varchar(100) DEFAULT NULL,
  `csv_dumpcol49` varchar(100) DEFAULT NULL,
  `csv_dumpcol50` varchar(100) DEFAULT NULL,
  `csv_dumpcol51` varchar(100) DEFAULT NULL,
  `csv_dumpcol52` varchar(100) DEFAULT NULL,
  `csv_dumpcol53` varchar(100) DEFAULT NULL,
  `csv_dumpcol54` varchar(100) DEFAULT NULL,
  `csv_dumpcol55` varchar(100) DEFAULT NULL,
  `csv_dumpcol56` varchar(100) DEFAULT NULL,
  `csv_dumpcol57` varchar(100) DEFAULT NULL,
  `csv_dumpcol58` varchar(100) DEFAULT NULL,
  `csv_dumpcol59` varchar(100) DEFAULT NULL,
  `csv_dumpcol60` varchar(100) DEFAULT NULL,
  `csv_dumpcol61` varchar(100) DEFAULT NULL,
  `csv_dumpcol62` varchar(100) DEFAULT NULL,
  `csv_dumpcol63` varchar(100) DEFAULT NULL,
  `csv_dumpcol64` varchar(100) DEFAULT NULL,
  `csv_dumpcol65` varchar(100) DEFAULT NULL,
  `csv_dumpcol66` varchar(100) DEFAULT NULL,
  `csv_dumpcol67` varchar(100) DEFAULT NULL,
  `csv_dumpcol68` varchar(100) DEFAULT NULL,
  `csv_dumpcol69` varchar(100) DEFAULT NULL,
  `csv_dumpcol70` varchar(100) DEFAULT NULL,
  `csv_dumpcol71` varchar(100) DEFAULT NULL,
  `csv_dumpcol72` varchar(100) DEFAULT NULL,
  `csv_dumpcol73` varchar(100) DEFAULT NULL,
  `csv_dumpcol74` varchar(100) DEFAULT NULL,
  `csv_dumpcol75` varchar(100) DEFAULT NULL,
  `csv_dumpcol76` varchar(100) DEFAULT NULL,
  `csv_dumpcol77` varchar(100) DEFAULT NULL,
  `csv_dumpcol78` varchar(100) DEFAULT NULL,
  `csv_dumpcol79` varchar(100) DEFAULT NULL,
  `csv_dumpcol80` varchar(100) DEFAULT NULL,
  `csv_dumpcol81` varchar(100) DEFAULT NULL,
  `csv_dumpcol82` varchar(100) DEFAULT NULL,
  `csv_dumpcol83` varchar(100) DEFAULT NULL,
  `csv_dumpcol84` varchar(100) DEFAULT NULL,
  `csv_dumpcol85` varchar(100) DEFAULT NULL,
  `csv_dumpcol86` varchar(100) DEFAULT NULL,
  `csv_dumpcol87` varchar(100) DEFAULT NULL,
  `csv_dumpcol88` varchar(100) DEFAULT NULL,
  `csv_dumpcol89` varchar(100) DEFAULT NULL,
  `csv_dumpcol90` varchar(100) DEFAULT NULL,
  `csv_dumpcol91` varchar(100) DEFAULT NULL,
  `csv_dumpcol92` varchar(100) DEFAULT NULL,
  `csv_dumpcol93` varchar(100) DEFAULT NULL,
  `csv_dumpcol94` varchar(100) DEFAULT NULL,
  `csv_dumpcol95` varchar(100) DEFAULT NULL,
  `csv_dumpcol96` varchar(100) DEFAULT NULL,
  `csv_dumpcol97` varchar(100) DEFAULT NULL,
  `csv_dumpcol98` varchar(100) DEFAULT NULL,
  `csv_dumpcol99` varchar(100) DEFAULT NULL,
  `csv_dumpcol100` varchar(100) DEFAULT NULL,
  `csv_dumpcol101` varchar(100) DEFAULT NULL,
  `csv_dumpcol102` varchar(100) DEFAULT NULL,
  `csv_dumpcol103` varchar(100) DEFAULT NULL,
  `csv_dumpcol104` varchar(100) DEFAULT NULL,
  `csv_dumpcol105` varchar(100) DEFAULT NULL,
  `csv_dumpcol106` varchar(100) DEFAULT NULL,
  `csv_dumpcol107` varchar(100) DEFAULT NULL,
  `csv_dumpcol108` varchar(100) DEFAULT NULL,
  `csv_dumpcol109` varchar(100) DEFAULT NULL,
  `csv_dumpcol110` varchar(100) DEFAULT NULL,
  `csv_dumpcol111` varchar(100) DEFAULT NULL,
  `csv_dumpcol112` varchar(100) DEFAULT NULL,
  `csv_dumpcol113` varchar(100) DEFAULT NULL,
  `csv_dumpcol114` varchar(100) DEFAULT NULL,
  `csv_dumpcol115` varchar(100) DEFAULT NULL,
  `csv_dumpcol116` varchar(100) DEFAULT NULL,
  `csv_dumpcol117` varchar(100) DEFAULT NULL,
  `csv_dumpcol118` varchar(100) DEFAULT NULL,
  `csv_dumpcol119` varchar(100) DEFAULT NULL,
  `csv_dumpcol120` varchar(100) DEFAULT NULL,
  `csv_dumpcol121` varchar(100) DEFAULT NULL,
  `csv_dumpcol122` varchar(100) DEFAULT NULL,
  `csv_dumpcol123` varchar(100) DEFAULT NULL,
  `csv_dumpcol124` varchar(100) DEFAULT NULL,
  `csv_dumpcol125` varchar(100) DEFAULT NULL,
  `csv_dumpcol126` varchar(100) DEFAULT NULL,
  `csv_dumpcol127` varchar(100) DEFAULT NULL,
  `csv_dumpcol128` varchar(100) DEFAULT NULL,
  `csv_dumpcol129` varchar(100) DEFAULT NULL,
  `csv_dumpcol130` varchar(100) DEFAULT NULL,
  `csv_dumpcol131` varchar(100) DEFAULT NULL,
  `csv_dumpcol132` varchar(100) DEFAULT NULL,
  `csv_dumpcol133` varchar(100) DEFAULT NULL,
  `csv_dumpcol134` varchar(100) DEFAULT NULL,
  `csv_dumpcol135` varchar(100) DEFAULT NULL,
  `csv_dumpcol136` varchar(100) DEFAULT NULL,
  `csv_dumpcol137` varchar(100) DEFAULT NULL,
  `csv_dumpcol138` varchar(100) DEFAULT NULL,
  `csv_dumpcol139` varchar(100) DEFAULT NULL,
  `csv_dumpcol140` varchar(100) DEFAULT NULL,
  `csv_dumpcol141` varchar(100) DEFAULT NULL,
  `csv_dumpcol142` varchar(100) DEFAULT NULL,
  `csv_dumpcol143` varchar(100) DEFAULT NULL,
  `csv_dumpcol144` varchar(100) DEFAULT NULL,
  `csv_dumpcol145` varchar(100) DEFAULT NULL,
  `csv_dumpcol146` varchar(100) DEFAULT NULL,
  `csv_dumpcol147` varchar(100) DEFAULT NULL,
  `csv_dumpcol148` varchar(100) DEFAULT NULL,
  `csv_dumpcol149` varchar(100) DEFAULT NULL,
  `csv_dumpcol150` varchar(100) DEFAULT NULL,
  `csv_dumpcol151` varchar(100) DEFAULT NULL,
  `csv_dumpcol152` varchar(100) DEFAULT NULL,
  `csv_dumpcol153` varchar(100) DEFAULT NULL,
  `csv_dumpcol154` varchar(100) DEFAULT NULL,
  `csv_dumpcol155` varchar(100) DEFAULT NULL,
  `csv_dumpcol156` varchar(100) DEFAULT NULL,
  `csv_dumpcol157` varchar(100) DEFAULT NULL,
  `csv_dumpcol158` varchar(100) DEFAULT NULL,
  `csv_dumpcol159` varchar(100) DEFAULT NULL,
  `csv_dumpcol160` varchar(100) DEFAULT NULL,
  `csv_dumpcol161` varchar(100) DEFAULT NULL,
  `csv_dumpcol162` varchar(100) DEFAULT NULL,
  `csv_dumpcol163` varchar(100) DEFAULT NULL,
  `csv_dumpcol164` varchar(100) DEFAULT NULL,
  `csv_dumpcol165` varchar(100) DEFAULT NULL,
  `csv_dumpcol166` varchar(100) DEFAULT NULL,
  `csv_dumpcol167` varchar(100) DEFAULT NULL,
  `csv_dumpcol168` varchar(100) DEFAULT NULL,
  `csv_dumpcol169` varchar(100) DEFAULT NULL,
  `csv_dumpcol170` varchar(100) DEFAULT NULL,
  `csv_dumpcol171` varchar(100) DEFAULT NULL,
  `csv_dumpcol172` varchar(100) DEFAULT NULL,
  `csv_dumpcol173` varchar(100) DEFAULT NULL,
  `csv_dumpcol174` varchar(100) DEFAULT NULL,
  `csv_dumpcol175` varchar(100) DEFAULT NULL,
  `csv_dumpcol176` varchar(100) DEFAULT NULL,
  `csv_dumpcol177` varchar(100) DEFAULT NULL,
  `csv_dumpcol178` varchar(100) DEFAULT NULL,
  `csv_dumpcol179` varchar(100) DEFAULT NULL,
  `csv_dumpcol180` varchar(100) DEFAULT NULL,
  `csv_dumpcol181` varchar(100) DEFAULT NULL,
  `csv_dumpcol182` varchar(100) DEFAULT NULL,
  `csv_dumpcol183` varchar(100) DEFAULT NULL,
  `csv_dumpcol184` varchar(100) DEFAULT NULL,
  `csv_dumpcol185` varchar(100) DEFAULT NULL,
  `csv_dumpcol186` varchar(100) DEFAULT NULL,
  `csv_dumpcol187` varchar(100) DEFAULT NULL,
  `csv_dumpcol188` varchar(100) DEFAULT NULL,
  `csv_dumpcol189` varchar(100) DEFAULT NULL,
  `csv_dumpcol190` varchar(100) DEFAULT NULL,
  `csv_dumpcol191` varchar(100) DEFAULT NULL,
  `csv_dumpcol192` varchar(100) DEFAULT NULL,
  `csv_dumpcol193` varchar(100) DEFAULT NULL,
  `csv_dumpcol194` varchar(100) DEFAULT NULL,
  `csv_dumpcol195` varchar(100) DEFAULT NULL,
  `csv_dumpcol196` varchar(100) DEFAULT NULL,
  `csv_dumpcol197` varchar(100) DEFAULT NULL,
  `csv_dumpcol198` varchar(100) DEFAULT NULL,
  `csv_dumpcol199` varchar(100) DEFAULT NULL,
  `csv_dumpcol200` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`column0pk`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `csv_dump`
--

LOCK TABLES `csv_dump` WRITE;
/*!40000 ALTER TABLE `csv_dump` DISABLE KEYS */;
/*!40000 ALTER TABLE `csv_dump` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `csv_dump_extended`
--

DROP TABLE IF EXISTS `csv_dump_extended`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `csv_dump_extended` (
  `column0pk` int(11) NOT NULL AUTO_INCREMENT,
  `csv_dumpcol201` varchar(100) DEFAULT NULL,
  `csv_dumpcol202` varchar(100) DEFAULT NULL,
  `csv_dumpcol203` varchar(100) DEFAULT NULL,
  `csv_dumpcol204` varchar(100) DEFAULT NULL,
  `csv_dumpcol205` varchar(100) DEFAULT NULL,
  `csv_dumpcol206` varchar(100) DEFAULT NULL,
  `csv_dumpcol207` varchar(100) DEFAULT NULL,
  `csv_dumpcol208` varchar(100) DEFAULT NULL,
  `csv_dumpcol209` varchar(100) DEFAULT NULL,
  `csv_dumpcol210` varchar(100) DEFAULT NULL,
  `csv_dumpcol211` varchar(100) DEFAULT NULL,
  `csv_dumpcol212` varchar(100) DEFAULT NULL,
  `csv_dumpcol213` varchar(100) DEFAULT NULL,
  `csv_dumpcol214` varchar(100) DEFAULT NULL,
  `csv_dumpcol215` varchar(100) DEFAULT NULL,
  `csv_dumpcol216` varchar(100) DEFAULT NULL,
  `csv_dumpcol217` varchar(100) DEFAULT NULL,
  `csv_dumpcol218` varchar(100) DEFAULT NULL,
  `csv_dumpcol219` varchar(100) DEFAULT NULL,
  `csv_dumpcol220` varchar(100) DEFAULT NULL,
  `csv_dumpcol221` varchar(100) DEFAULT NULL,
  `csv_dumpcol222` varchar(100) DEFAULT NULL,
  `csv_dumpcol223` varchar(100) DEFAULT NULL,
  `csv_dumpcol224` varchar(100) DEFAULT NULL,
  `csv_dumpcol225` varchar(100) DEFAULT NULL,
  `csv_dumpcol226` varchar(100) DEFAULT NULL,
  `csv_dumpcol227` varchar(100) DEFAULT NULL,
  `csv_dumpcol228` varchar(100) DEFAULT NULL,
  `csv_dumpcol229` varchar(100) DEFAULT NULL,
  `csv_dumpcol230` varchar(100) DEFAULT NULL,
  `csv_dumpcol231` varchar(100) DEFAULT NULL,
  `csv_dumpcol232` varchar(100) DEFAULT NULL,
  `csv_dumpcol233` varchar(100) DEFAULT NULL,
  `csv_dumpcol234` varchar(100) DEFAULT NULL,
  `csv_dumpcol235` varchar(100) DEFAULT NULL,
  `csv_dumpcol236` varchar(100) DEFAULT NULL,
  `csv_dumpcol237` varchar(100) DEFAULT NULL,
  `csv_dumpcol238` varchar(100) DEFAULT NULL,
  `csv_dumpcol239` varchar(100) DEFAULT NULL,
  `csv_dumpcol240` varchar(100) DEFAULT NULL,
  `csv_dumpcol241` varchar(100) DEFAULT NULL,
  `csv_dumpcol242` varchar(100) DEFAULT NULL,
  `csv_dumpcol243` varchar(100) DEFAULT NULL,
  `csv_dumpcol244` varchar(100) DEFAULT NULL,
  `csv_dumpcol245` varchar(100) DEFAULT NULL,
  `csv_dumpcol246` varchar(100) DEFAULT NULL,
  `csv_dumpcol247` varchar(100) DEFAULT NULL,
  `csv_dumpcol248` varchar(100) DEFAULT NULL,
  `csv_dumpcol249` varchar(100) DEFAULT NULL,
  `csv_dumpcol250` varchar(100) DEFAULT NULL,
  `csv_dumpcol251` varchar(100) DEFAULT NULL,
  `csv_dumpcol252` varchar(100) DEFAULT NULL,
  `csv_dumpcol253` varchar(100) DEFAULT NULL,
  `csv_dumpcol254` varchar(100) DEFAULT NULL,
  `csv_dumpcol255` varchar(100) DEFAULT NULL,
  `csv_dumpcol256` varchar(100) DEFAULT NULL,
  `csv_dumpcol257` varchar(100) DEFAULT NULL,
  `csv_dumpcol258` varchar(100) DEFAULT NULL,
  `csv_dumpcol259` varchar(100) DEFAULT NULL,
  `csv_dumpcol260` varchar(100) DEFAULT NULL,
  `csv_dumpcol261` varchar(100) DEFAULT NULL,
  `csv_dumpcol262` varchar(100) DEFAULT NULL,
  `csv_dumpcol263` varchar(100) DEFAULT NULL,
  `csv_dumpcol264` varchar(100) DEFAULT NULL,
  `csv_dumpcol265` varchar(100) DEFAULT NULL,
  `csv_dumpcol266` varchar(100) DEFAULT NULL,
  `csv_dumpcol267` varchar(100) DEFAULT NULL,
  `csv_dumpcol268` varchar(100) DEFAULT NULL,
  `csv_dumpcol269` varchar(100) DEFAULT NULL,
  `csv_dumpcol270` varchar(100) DEFAULT NULL,
  `csv_dumpcol271` varchar(100) DEFAULT NULL,
  `csv_dumpcol272` varchar(100) DEFAULT NULL,
  `csv_dumpcol273` varchar(100) DEFAULT NULL,
  `csv_dumpcol274` varchar(100) DEFAULT NULL,
  `csv_dumpcol275` varchar(100) DEFAULT NULL,
  `csv_dumpcol276` varchar(100) DEFAULT NULL,
  `csv_dumpcol277` varchar(100) DEFAULT NULL,
  `csv_dumpcol278` varchar(100) DEFAULT NULL,
  `csv_dumpcol279` varchar(100) DEFAULT NULL,
  `csv_dumpcol280` varchar(100) DEFAULT NULL,
  `csv_dumpcol281` varchar(100) DEFAULT NULL,
  `csv_dumpcol282` varchar(100) DEFAULT NULL,
  `csv_dumpcol283` varchar(100) DEFAULT NULL,
  `csv_dumpcol284` varchar(100) DEFAULT NULL,
  `csv_dumpcol285` varchar(100) DEFAULT NULL,
  `csv_dumpcol286` varchar(100) DEFAULT NULL,
  `csv_dumpcol287` varchar(100) DEFAULT NULL,
  `csv_dumpcol288` varchar(100) DEFAULT NULL,
  `csv_dumpcol289` varchar(100) DEFAULT NULL,
  `csv_dumpcol290` varchar(100) DEFAULT NULL,
  `csv_dumpcol291` varchar(100) DEFAULT NULL,
  `csv_dumpcol292` varchar(100) DEFAULT NULL,
  `csv_dumpcol293` varchar(100) DEFAULT NULL,
  `csv_dumpcol294` varchar(100) DEFAULT NULL,
  `csv_dumpcol295` varchar(100) DEFAULT NULL,
  `csv_dumpcol296` varchar(100) DEFAULT NULL,
  `csv_dumpcol297` varchar(100) DEFAULT NULL,
  `csv_dumpcol298` varchar(100) DEFAULT NULL,
  `csv_dumpcol299` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`column0pk`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `csv_dump_extended`
--

LOCK TABLES `csv_dump_extended` WRITE;
/*!40000 ALTER TABLE `csv_dump_extended` DISABLE KEYS */;
/*!40000 ALTER TABLE `csv_dump_extended` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parcel_metadata`
--

DROP TABLE IF EXISTS `parcel_metadata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parcel_metadata` (
  `header_id` int(11) NOT NULL AUTO_INCREMENT,
  `header_name` varchar(100) DEFAULT NULL,
  `header_type` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`header_id`),
  UNIQUE KEY `header_name_UNIQUE` (`header_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parcel_metadata`
--

LOCK TABLES `parcel_metadata` WRITE;
/*!40000 ALTER TABLE `parcel_metadata` DISABLE KEYS */;
/*!40000 ALTER TABLE `parcel_metadata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parcel_values`
--

DROP TABLE IF EXISTS `parcel_values`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parcel_values` (
  `value_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `header_id` int(11) DEFAULT NULL,
  `object_id` int(11) DEFAULT NULL,
  `parcel_value` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`value_id`),
  KEY `user_id_idx` (`user_id`),
  KEY `object_id_idx` (`object_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parcel_values`
--

LOCK TABLES `parcel_values` WRITE;
/*!40000 ALTER TABLE `parcel_values` DISABLE KEYS */;
/*!40000 ALTER TABLE `parcel_values` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `point_configuration`
--

DROP TABLE IF EXISTS `point_configuration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `point_configuration` (
  `config_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `row_count` int(11) DEFAULT NULL,
  `header_id` int(11) DEFAULT NULL,
  `point_condition` varchar(45) DEFAULT NULL,
  `point_value` varchar(100) DEFAULT NULL,
  `points` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`config_id`),
  UNIQUE KEY `user_header` (`user_id`,`row_count`),
  KEY `user_id_idx` (`user_id`),
  KEY `header_id_idx` (`header_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `point_configuration`
--

LOCK TABLES `point_configuration` WRITE;
/*!40000 ALTER TABLE `point_configuration` DISABLE KEYS */;
/*!40000 ALTER TABLE `point_configuration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `search_criteria`
--

DROP TABLE IF EXISTS `search_criteria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `search_criteria` (
  `search_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `header_id` int(11) DEFAULT NULL,
  `search_condition` varchar(10) DEFAULT NULL,
  `search_value` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`search_id`),
  KEY `search_user_id_idx` (`user_id`),
  KEY `search_header_id_idx` (`header_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `search_criteria`
--

LOCK TABLES `search_criteria` WRITE;
/*!40000 ALTER TABLE `search_criteria` DISABLE KEYS */;
/*!40000 ALTER TABLE `search_criteria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_table`
--

DROP TABLE IF EXISTS `user_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_table` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_email` varchar(100) DEFAULT NULL,
  `user_first_name` varchar(100) DEFAULT NULL,
  `user_last_name` varchar(100) DEFAULT NULL,
  `user_password` varchar(100) DEFAULT NULL,
  `user_admin` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_table`
--

LOCK TABLES `user_table` WRITE;
/*!40000 ALTER TABLE `user_table` DISABLE KEYS */;
INSERT INTO `user_table` VALUES (1,'pra','Pranav','Patil','pra',1),(2,'andrew@gmail.com','Andrew','Johnson','andrew',1);
/*!40000 ALTER TABLE `user_table` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-09-07 14:44:58
