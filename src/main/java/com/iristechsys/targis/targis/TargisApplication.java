package com.iristechsys.targis.targis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.iristechsys.targis")
public class TargisApplication {

	public static void main(String[] args) {
		SpringApplication.run(TargisApplication.class, args);
		
		//FileUtility file = new FileUtility();
		//file.readParcelFile("E://GIS_workspace_STS/Data/Parcels.csv");
		
	}
}
