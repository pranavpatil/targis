package com.iristechsys.targis.targis;

import java.io.BufferedReader;
import java.io.FileReader;

import org.springframework.stereotype.Repository;

@Repository
public class FileUtility {
	static  Integer counter = 0;
	DatabaseConnection databaseConnection = new DatabaseConnection();
	
	public void readParcelFile(String fileName){
		String line=null;
		
		try {
			
			databaseConnection.getConnected();
			
			FileReader fileReader = new FileReader(fileName);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			
			line=bufferedReader.readLine(); //Read first line of file
			String[] headerList = line.split(",");
			
			String[] tablesToTruncate= {"parcel_metadata", "csv_dump", "csv_dump_extended"};
			truncateTables(tablesToTruncate);
			
			insertMetaData(headerList); // Pass headerList to insert 
			long startTime = System.currentTimeMillis();
			
			int headerLength= headerList.length;
			System.out.println(headerLength);
			
			while((line=bufferedReader.readLine())!= null){
				
				String[] dataList = line.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");
				
				if(dataList.length == headerLength){
					// Pass dataList to insert 
					insertData(dataList); 
			    }else {
			    	System.out.println(dataList.length);
			    }
				
			}
			
			long endTime = System.currentTimeMillis();
			System.out.println("start time "+startTime+"end time "+endTime);
			long allTime = startTime - endTime;
			System.out.println("all time "+allTime);
			
			bufferedReader.close();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void truncateTables(String[] tableName) {
		
		for(int i=0; i<tableName.length; i++) {
			databaseConnection.resetTableData(tableName[i]);
		}
		
	}
	
	public void insertMetaData(String[] headerList){
		
		try {
			
			for(int i=0; i<headerList.length; i++){
				databaseConnection.insertParcelMeta(headerList[i]);
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
				
	}
	
	public void insertDataOld(String[] dataList){
		try {
			
			for(int i=0; i<dataList.length; i++){
				//System.out.println(headerList[i]);
				int userId=1;				
				int headerId=i+1;
				String objectId= dataList[0];
				String parcelValue=dataList[i];
				System.out.println(userId+" "+headerId+" "+objectId+" "+parcelValue);
				databaseConnection.insertParcelValue(userId, headerId, objectId, parcelValue);
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public void insertData(String[] dataList){
		try {
			StringBuffer queryStringPartI = new StringBuffer();
			queryStringPartI.append("insert into gis.csv_dump (");
			
			StringBuffer queryStringPartII = new StringBuffer();
			queryStringPartII.append(" values(");
			
			for(int i=1; i<=200; i++)
			{
				//returnQuery(1, dataList[i]);
				queryStringPartI.append("csv_dumpcol"+i);
				
				String dataString = dataList[i-1];
				
				dataString = dataString.replace("'", "");
				
				if (dataString.length()>98)
				{
					dataString = dataString.substring(0, 98);
				}
				
				queryStringPartII.append("'"+dataString+"'");
				
				if(i<200)
				{
					queryStringPartI.append(",");	
					queryStringPartII.append(",");					

				}				
			}
			
			queryStringPartI.append(")");
			queryStringPartII.append(")");
			

		String finalString = queryStringPartI.toString() +  queryStringPartII.toString();
		//System.out.println(finalString);
		databaseConnection.insertParcelValueToCSVDumpTables(finalString);
            
		queryStringPartI = new StringBuffer();
		queryStringPartI.append("insert into gis.csv_dump_extended (");
		queryStringPartII = new StringBuffer();
		queryStringPartII.append(" values(");

		
			for(int i=201; i<dataList.length; i++){
				
				queryStringPartI.append("csv_dumpcol"+i);

				String dataString = dataList[i-1];
				dataString = dataString.replace("'", "");
				
				if (dataString.length()>98)
				{
					dataString = dataString.substring(0, 98);
				}
				
				queryStringPartII.append("'"+dataList[i-1]+"'");
				
				if(i<dataList.length-1)
				{
					queryStringPartI.append(",");
					queryStringPartII.append(",");
				}				
				//System.out.println(headerList[i]);
				
			}
			queryStringPartI.append(")");
			queryStringPartII.append(")");
			String finalStringII = queryStringPartI.toString() +  queryStringPartII.toString();
			//System.out.println(finalStringII);
			databaseConnection.insertParcelValueToCSVDumpTables(finalStringII);

			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
			
	
}
