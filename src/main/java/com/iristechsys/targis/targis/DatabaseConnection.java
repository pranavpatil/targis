package com.iristechsys.targis.targis;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Repository;
@Repository
public class DatabaseConnection {
	
	private Connection connect = null;
    private Statement statement = null;
    private PreparedStatement preparedStatement = null;
    private ResultSet resultSet = null;
    
    
    public void getConnected() throws ClassNotFoundException{
    	
    	try {
    	Class.forName("com.mysql.cj.jdbc.Driver");
        	connect = DriverManager.getConnection("jdbc:mysql://localhost:3306/gis?"
			                + "user=root&password=root");
	                			     
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}        
    }

    public void resetTableData(String tableName) {
    	try {
			preparedStatement = connect.prepareStatement("TRUNCATE TABLE "+tableName);
			preparedStatement.executeUpdate();
			System.out.println(tableName+" truncated Succefully");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    public void insertParcelMeta(String headerName){
    	try {
			preparedStatement = connect
					.prepareStatement("insert into gis.parcel_metadata values (default, ?, default)");
				preparedStatement.setString(1, headerName);
				preparedStatement.executeUpdate();
    	} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    public Map<String, String> getHeaderData(){
    	
    	Map<String, String> hederMap =  new HashMap<>();
    	try {
			statement = connect.createStatement();
			resultSet = statement.executeQuery("select * from gis.parcel_metadata");
	        while (resultSet.next()) {
	        	String  headerName = resultSet.getString("header_name");	
	        	String  headerId = resultSet.getString("header_id");	

	        	hederMap.put(headerId, headerName);
	         }
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return hederMap;
    	
    }
    
    
    public void insertParcelValue(int userId, int headerId, String objectId, String parcelValue){
    	try {
    		preparedStatement = connect
				.prepareStatement("insert into gis.parcel_values values (default, ?, ?, ?, ?)");
				preparedStatement.setInt(1, userId);
				preparedStatement.setInt(2, headerId);
				preparedStatement.setString(3, objectId);
				preparedStatement.setString(4, parcelValue);
				preparedStatement.executeUpdate();
				
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    
    public Map<Integer, Map<String, String>> getParcelData(String parcelStatement){
    	Map<Integer, Map<String, String>> objectMap =  new HashMap<>();
    	Map<String, String> valueMap=null;
    	int objectId=0;
    	
    	try {
			statement = connect.createStatement();
			resultSet = statement.executeQuery(parcelStatement);
			ResultSetMetaData md = resultSet.getMetaData();
			int columns = md.getColumnCount();
			while (resultSet.next()) 
			{
				objectId= resultSet.getInt("csv_dumpcol1");
				valueMap =  new HashMap<>();

				valueMap.put("totalpoints", "20");
				
				for (int i=2; i<=columns; i++){
					    String column_name = md.getColumnName(i);
					    valueMap.put(column_name, resultSet.getString(column_name));
				}
				
		       	objectMap.put(objectId, valueMap);
			}
    	} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return objectMap;
    	
	}
	
	public Map<String, String> getParcelDataById(String parcelStatement){
		Map<String, String> valueMap =  new HashMap<>();
		
		try {
			statement = connect.createStatement();
			resultSet = statement.executeQuery(parcelStatement);
			ResultSetMetaData md = resultSet.getMetaData();
			int columns = md.getColumnCount();
			while (resultSet.next()) 
			{
				String totalpoints= "25";
				valueMap.put("totalpoints", totalpoints);
				for (int i=1; i<=columns; i++){
					    String column_name = md.getColumnName(i);
					    valueMap.put(column_name, resultSet.getString(column_name));
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return valueMap;
		
	}

	public String insertSearch(String insertStatement){
    	int count =0;	
    	try {
				preparedStatement = connect
					.prepareStatement(insertStatement);
				count = preparedStatement.executeUpdate();
    		
    	} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
    	}
    	
    	return "Total "+count+" item inserted";
    }
	
	public Map<Integer, Map<String, String>> getSearch(String parcelStatement){
    	Map<String, String> valueMap =  new HashMap<>();
    	Map<Integer, Map<String, String>> objectMap =  new HashMap<>();
    	
    	int objectId=0;
    	try {
			statement = connect.createStatement();
			resultSet = statement.executeQuery(parcelStatement);
			ResultSetMetaData md = resultSet.getMetaData();
			int columns = md.getColumnCount();
			
			while (resultSet.next()) 
			{
				objectId = resultSet.getInt("search_id");
				valueMap =  new HashMap<>();
				
				for (int i=2; i<=columns; i++){
					    String column_name = md.getColumnName(i);
					    valueMap.put(column_name, resultSet.getString(column_name));
				}
				
				objectMap.put(objectId, valueMap);
			}
    	} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return objectMap;
    	
	}
	
	public String insertPoints(String insertStatement){
    	int count =0;	
    	try {
				preparedStatement = connect
					.prepareStatement(insertStatement);
				count = preparedStatement.executeUpdate();
    		
    	} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
    	}
    	
    	return "Total "+count+" item inserted";
    }
	
	public ArrayList<Map<String, String>> getPoints(String parcelStatement){
    	Map<String, String> valueMap =  new HashMap<>();
    	ArrayList<Map<String, String>> pointsList =new ArrayList<>();
    	
    	try {
			statement = connect.createStatement();
			resultSet = statement.executeQuery(parcelStatement);
			ResultSetMetaData md = resultSet.getMetaData();
			int columns = md.getColumnCount();
			while (resultSet.next()) 
			{
				valueMap =  new HashMap<>();
				for (int i=1; i<=columns; i++){
					    String column_name = md.getColumnName(i);
					    String column_name_alt ="";
					    
					    if(column_name.equals("criteriaName")) {
					    	column_name_alt="criteriaName";
					    }
					    
					    if(column_name.equals("point_condition")) {
					    	column_name_alt="criteriaCondition";
					    }
					    
					    if(column_name.equals("point_value")) {
					    	column_name_alt="criteriaValue";
					    }
					    
					    if(column_name.equals("points")) {
					    	column_name_alt="criteriaPoints";
					    }
					    
					    valueMap.put(column_name_alt, resultSet.getString(column_name));
				}
				
				pointsList.add(valueMap);
			}
			
			for(int i=columns+1; i<=10; i++) {
				valueMap =  new HashMap<>();
				valueMap.put("criteriaName", "");
				valueMap.put("criteriaCondition", "");
				valueMap.put("criteriaValue", "");
				valueMap.put("criteriaPoints", "");
				pointsList.add(valueMap);
			}
			
    	} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return pointsList;
    	
	}   
	
	public boolean userLogin(String userName, String password){
	    	boolean checkUserExist = false;
	    	
	    	try {
				statement = connect.createStatement();
				resultSet = statement.executeQuery("select * from gis.user_table where user_email = '"+userName+"' and user_password = '"+password+"' limit 1");
				//ResultSetMetaData md = resultSet.getMetaData();
				//int columns = md.getColumnCount();
		        
				if(resultSet.next())
				{
					checkUserExist = true;
				}
				else
				{
					checkUserExist = false;
				}
				

			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return checkUserExist;
	    	
	}
	
	public Map<String, String> userDetails(String userStatement){
    	
    	Map<String, String> hederMap =  new HashMap<>();
    	try {
			statement = connect.createStatement();
			resultSet = statement.executeQuery(userStatement);
			ResultSetMetaData md = resultSet.getMetaData();
			int columns = md.getColumnCount();
			
	        while (resultSet.next()) {
	        	
	        	for (int i=1; i<=columns; i++){
				    String column_name = md.getColumnName(i);
				    hederMap.put(column_name, resultSet.getString(column_name));
	        	}	        	
	         }
	        
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return hederMap;
    	
}
	
	public void closeConnection() throws SQLException {
		// TODO Auto-generated method stub
		connect.close();
	}
	
	  public void insertParcelValueToCSVDumpTables(String queryString){
	    	try 
	    	{
	    		statement = connect.createStatement();
	    		statement.executeUpdate(queryString);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    }


	public String userRegistration(String firstName, String lastName, String userEmail, String userPassword,
			boolean userType) {

		try {
			preparedStatement = connect
				.prepareStatement("insert into gis.user_table value (default, ?, ?, ?, ?, ?)");
			
			preparedStatement.setString(1, userEmail);
			preparedStatement.setString(2, firstName);
			preparedStatement.setString(3, lastName);
			preparedStatement.setString(4, userPassword);
			preparedStatement.setBoolean(4, userType);
			preparedStatement.executeUpdate();
		
	} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
	}
		
		return null;
	}	
}
