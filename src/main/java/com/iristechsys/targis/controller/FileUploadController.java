package com.iristechsys.targis.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.iristechsys.targis.targis.FileUtility;

@Controller
@ComponentScan("com")

public class FileUploadController
{
    @Autowired
    FileUtility fileUtility;
    
	@GetMapping("/uploadfile")
	public String redirectPage()
	{
		return "uploadfile.html";
	}
	
	@PostMapping("/upload")
	 public String uploadFile(@RequestParam("file") MultipartFile file)
	 {
		try 
		{ 
			if(!file.isEmpty())
			{
				System.out.println(file.getBytes());
				byte[] bytes = file.getBytes();
				Path path = Paths.get("F://fileupload/" + file.getOriginalFilename());
				Files.write(path, bytes);
				fileUtility.readParcelFile(path.toString());
			}
		} catch (IOException e) 
		{
			e.printStackTrace();
		}
		 return "uploadfile";
	 }
	
}
