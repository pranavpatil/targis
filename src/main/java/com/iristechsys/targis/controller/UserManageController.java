package com.iristechsys.targis.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.iristechsys.targis.targis.DatabaseConnection;

@Controller
@Component
@CrossOrigin(origins = "*", allowedHeaders = "*")

public class UserManageController {

	@Autowired
	DatabaseConnection databaseConnection;
	
	
	/*@PostMapping("/userLogin",consumes = "text/plain")*/
	@RequestMapping(
			   value = "/userLogin",
			   method = RequestMethod.POST,
			   consumes = "text/plain")
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	public ResponseEntity<String> userLogin(@RequestBody String payload) throws ClassNotFoundException, JsonProcessingException, JSONException 
	{
		databaseConnection.getConnected();
		//user credential format
		//{"username":"amolmagar1000@gmail.com", "password":"admin"}
		
		//JSONObject jsonObject= null;
		String userName = null, password = null;
		System.out.println(payload);
		
		try {
			JSONObject jsonObject = new JSONObject(payload);
			userName = jsonObject.getString("username");
			password= jsonObject.getString("password");
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		boolean jsonResponse= databaseConnection.userLogin(userName, password);
		
		String loginMessage;
		if(jsonResponse)
		{
			loginMessage = "SUCCESS";
		}
		else
		{
			loginMessage = "FAIL";
		}
		
		//String responseString = new ObjectMapper().writeValueAsString(jsonResponse);
		//JSONObject jsonObject =  new JSONObject();
		//jsonObject.put("loginMessage", loginMessage);
		ObjectMapper objectMapper = new ObjectMapper();
		String json = objectMapper.writeValueAsString(loginMessage);
		
        return new ResponseEntity<String>(json , HttpStatus.OK);
	}
	
	@GetMapping("/getUserDetails")
	public ResponseEntity<String> getUserDetails(HttpServletRequest request) throws JsonProcessingException, ClassNotFoundException 
	{
		String userStatement = "select * FROM gis.user_table WHERE";
		userStatement += " gis.user_table.user_email = '"+request.getHeader("email").toString()+"'";
		
        System.out.println(userStatement);
		
	    databaseConnection.getConnected();
		Map<String, String> parcelValues = databaseConnection.userDetails(userStatement);
		String json = new ObjectMapper().writeValueAsString(parcelValues);
		
        return new ResponseEntity<String>(json, HttpStatus.OK);
	}
	
	@RequestMapping(value="/userRegistration", method = RequestMethod.POST, consumes = "text/plain")
	public ResponseEntity<String> userRegistration(@RequestBody String userData) throws ClassNotFoundException
	{
		databaseConnection.getConnected();
		String firstName = null, LastName = null, userEmail = null, userPassword = null;
		boolean userType = false;
		try {
			JSONObject userDetails = new JSONObject(userData);
			firstName = userDetails.getString("firstName");
			LastName = userDetails.getString("lastName");
			userEmail = userDetails.getString("userEmail");
			userPassword = userDetails.getString("userPassword");
			userType = userDetails.getBoolean("userType");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String userRegistration = databaseConnection.userRegistration(firstName, LastName, userEmail, userPassword, userType);
		return new ResponseEntity<String>(userRegistration, HttpStatus.OK);		
	}
}
