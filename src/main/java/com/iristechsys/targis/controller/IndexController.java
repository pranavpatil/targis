package com.iristechsys.targis.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import com.iristechsys.targis.targis.FileUtility;

@Controller
@ComponentScan("com")

public class IndexController
{

    
	@GetMapping("/index")
	public String redirectPage()
	{
		return "index";
	}
	
}
