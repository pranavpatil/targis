package com.iristechsys.targis.controller;

import java.util.ArrayList;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.iristechsys.targis.targis.DatabaseConnection;



@Controller
@Component
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class GetHeaderController 
{
	@Autowired
	DatabaseConnection databaseConnection;
	
	@GetMapping("/getHeader")
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	public ResponseEntity<String> getHeader() throws ClassNotFoundException, JsonProcessingException 
	{
		databaseConnection.getConnected();
		Map<String, String> headerName = databaseConnection.getHeaderData();
		String json = new ObjectMapper().writeValueAsString(headerName);
        return new ResponseEntity<String>(json, HttpStatus.OK);
	}
	
	@RequestMapping( 
				value = "/searchParcelValue",
				method = RequestMethod.POST,
				consumes = "text/plain")
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	public ResponseEntity<String> searchParcelValue(@RequestBody String request) throws JsonProcessingException, ClassNotFoundException 
	{
		//json required 
		//{"search":[{"criteriaName":"45:Jurisdiction","criteriaCondition":"=","criteriaValue":"Roanoke County"}, {"criteriaName":"2:Address_Number","criteriaCondition":"=","criteriaValue":"5620"}]}
		
		String parcelSelect = "select gis.csv_dump.column0pk, gis.csv_dump.csv_dumpcol1, gis.csv_dump.csv_dumpcol33, gis.csv_dump.csv_dumpcol45, gis.csv_dump.csv_dumpcol73, gis.csv_dump.csv_dumpcol74, gis.csv_dump.csv_dumpcol79, gis.csv_dump.csv_dumpcol91, gis.csv_dump.csv_dumpcol92, gis.csv_dump.csv_dumpcol97, gis.csv_dump.csv_dumpcol107 ";
		String parcelTables = " FROM gis.csv_dump, gis.csv_dump_extended";
		String parcelWhere = " WHERE gis.csv_dump.column0pk = gis.csv_dump_extended.column0pk ";
		String parcelCondition = " ORDER BY gis.csv_dump.column0pk, gis.csv_dump_extended.column0pk LIMIT 50";
		String andCondition = "";
		
		System.out.println(request);
		
		JSONObject jsonObj = null;
		
		try {
			jsonObj = new JSONObject(request);
			
			JSONArray searchItems = jsonObj.getJSONArray("search");
			for (int i = 0, size = searchItems.length(); i < size; i++)
			{
				JSONObject objectInArray = searchItems.getJSONObject(i);
				
				String criteriaName = objectInArray.get("criteriaName").toString();
				String criteria = objectInArray.get("criteriaCondition").toString();
				String value = objectInArray.get("criteriaValue").toString();
				String headerCriteria = "";
				andCondition = "";
				
				if (criteriaName != null && !criteriaName.isEmpty() && value != null && !value.isEmpty() && criteria != null && !criteria.isEmpty()) {
				
					String[] splitCriteria = criteriaName.split(":");
					int header = Integer.parseInt(splitCriteria[0]);
					
					if(i <= (size-1)){
						andCondition = " AND ";
					}
					
					if(header <= 200) {
						headerCriteria = "gis.csv_dump.csv_dumpcol" + header + criteria + "'" + value+ "'";					
					}else{
						headerCriteria = "gis.csv_dump_extended.csv_dumpcol" + header + criteria + "'"+ value+ "'";
					}
					
					parcelWhere += andCondition + headerCriteria;
				}
			}
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String parcelStatement = parcelSelect+parcelTables+parcelWhere+parcelCondition;
        System.out.println(parcelStatement);
		
	    databaseConnection.getConnected();
		Map<Integer, Map<String, String>> parcelValues = databaseConnection.getParcelData(parcelStatement);
		String json = new ObjectMapper().writeValueAsString(parcelValues);
		
        return new ResponseEntity<String>(json, HttpStatus.OK);
	}
	
	@GetMapping("/getParcelById")
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	public ResponseEntity<String> getParcelById(HttpServletRequest request) throws JsonProcessingException, ClassNotFoundException 
	{
		
		String parcelSelect = "select gis.csv_dump.csv_dumpcol1, gis.csv_dump.csv_dumpcol12, gis.csv_dump.csv_dumpcol13, gis.csv_dump.csv_dumpcol14, gis.csv_dump.csv_dumpcol15,gis.csv_dump.csv_dumpcol16, gis.csv_dump.csv_dumpcol17, gis.csv_dump.csv_dumpcol33, gis.csv_dump.csv_dumpcol45,	gis.csv_dump.csv_dumpcol49, gis.csv_dump.csv_dumpcol54, gis.csv_dump.csv_dumpcol55, gis.csv_dump.csv_dumpcol56,	gis.csv_dump.csv_dumpcol57,	gis.csv_dump.csv_dumpcol62, gis.csv_dump.csv_dumpcol70, gis.csv_dump.csv_dumpcol73, gis.csv_dump.csv_dumpcol79, gis.csv_dump.csv_dumpcol91, gis.csv_dump.csv_dumpcol107, gis.csv_dump.csv_dumpcol120, gis.csv_dump.csv_dumpcol124, gis.csv_dump.csv_dumpcol125, gis.csv_dump.csv_dumpcol126 ";
		String parcelTables = " FROM gis.csv_dump, gis.csv_dump_extended";
		String parcelWhere = " WHERE gis.csv_dump.column0pk = gis.csv_dump_extended.column0pk ";
		String parcelCondition = "";
		
		String parcelId = request.getParameter("parcelId").toString();
		
		parcelWhere += "AND gis.csv_dump.csv_dumpcol1='" + parcelId + "'";					
				
		String parcelStatement = parcelSelect + parcelTables + parcelWhere + parcelCondition;
        System.out.println(parcelStatement);
		
	    databaseConnection.getConnected();
		Map<String, String> parcelValues = databaseConnection.getParcelDataById(parcelStatement);
		String json = new ObjectMapper().writeValueAsString(parcelValues);
		
        return new ResponseEntity<String>(json, HttpStatus.OK);
	}
	
	@RequestMapping( 
			value = "/saveSearch",
			method = RequestMethod.POST,
			consumes = "text/plain")
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	public ResponseEntity<String> saveSearch(@RequestBody String request) throws JsonProcessingException, ClassNotFoundException 
	{
		//JSON Required 
		//{"saveSearch":[{"criteriaName":"45:Jurisdiction","criteriaCondition":"=","criteriaValue":"Roanoke County"}, {"criteriaName":"2:Address_Number","criteriaCondition":"=","criteriaValue":"5620"}], "userId":"1"}
		String saveSearchPartI= "insert into gis.search_criteria (user_id, header_id, search_condition, search_value) values ";
		String saveSearchPartII=""; 
		Boolean insertTrue=false;
		
		System.out.println(request);
		
		JSONObject jsonObj = null;
		
		try {
			jsonObj = new JSONObject(request);
			
			JSONArray searchItems = jsonObj.getJSONArray("savesearch");
			//String userId = jsonObj.getString("userId");
			String userId = "1";
			
			for (int i = 0, size = searchItems.length(); i < size; i++)
			{
				JSONObject objectInArray = searchItems.getJSONObject(i);
				
				String criteriaName = objectInArray.get("criteriaName").toString();
				String criteria = objectInArray.get("criteriaCondition").toString();
				String value = objectInArray.get("criteriaValue").toString();
				
				if (criteriaName != null && !criteriaName.isEmpty() && value != null && !value.isEmpty() && criteria != null && !criteria.isEmpty()) {
					String[] splitCriteria = criteriaName.split(":");
					int headerId = Integer.parseInt(splitCriteria[0]);
					
					saveSearchPartII += "("+userId+", "+headerId+", '"+criteria+"', '"+value+"'),";
					insertTrue=true;
					
				}
				
				if(i== (size-1)) {
					saveSearchPartII = saveSearchPartII.substring(0, saveSearchPartII.length() - 1);
				}
			}
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		//System.out.println(saveSearchPartI + saveSearchPartII);
		String parcelValues ="";
		
		if(insertTrue){
			databaseConnection.getConnected();
			parcelValues = databaseConnection.insertSearch(saveSearchPartI + saveSearchPartII);
		}else {
			parcelValues ="Please fill form before saving search.";
			System.out.println("Please fill form before saving search.");
		}
			
	    return new ResponseEntity<String>(parcelValues, HttpStatus.OK);
	}
	
	@GetMapping("/getSearchByUser")
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	public ResponseEntity<String> getSearchByUser(HttpServletRequest request) throws JsonProcessingException, ClassNotFoundException 
	{
		String userId = request.getParameter("userId").toString();
		
		String searchSelect = "SELECT gis.search_criteria.search_id, gis.search_criteria.header_id, gis.search_criteria.search_condition, gis.search_criteria.search_value ";
		String searchTables = "FROM gis.search_criteria ";
		String searchWhere = "WHERE gis.search_criteria.user_id = " + userId + "";					
		
		String searchStatement = searchSelect + searchTables + searchWhere ;
       // System.out.println(searchStatement);
		
	    databaseConnection.getConnected();
		Map<Integer, Map<String, String>> parcelValues = databaseConnection.getSearch(searchStatement);
		String json = new ObjectMapper().writeValueAsString(parcelValues);
		
        return new ResponseEntity<String>(json, HttpStatus.OK);
	}
	
	@RequestMapping( 
			value = "/savePointsConfiguration",
			method = RequestMethod.POST,
			consumes = "text/plain")
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	public ResponseEntity<String> savePointsConfiguration(@RequestBody String request) throws JsonProcessingException, ClassNotFoundException 
	{
		String savePointsPartI= "insert into gis.point_configuration (user_id, row_count, header_id, point_condition, point_value, points) values ";
		String savePointsPartII=""; 
		String savePointsCondition=" ON DUPLICATE KEY UPDATE header_id = values(header_id), point_condition = values(point_condition), point_value = values(point_value), points = values(points)";
		
		//System.out.println(request);
		
		Boolean insertTrue = false;
		
		JSONObject jsonObj = null;
		
		try {
			jsonObj = new JSONObject(request);
			
			JSONArray searchItems = jsonObj.getJSONArray("savepoints");
			//String userId = jsonObj.getString("userId");
			String userId = "1";
				
			for (int i = 0, size = searchItems.length(); i < size; i++)
			{
				JSONObject objectInArray = searchItems.getJSONObject(i);
				
				String criteriaName = objectInArray.get("criteriaName").toString();
				String criteria = objectInArray.get("criteriaCondition").toString();
				String value = objectInArray.get("criteriaValue").toString();
				String points = objectInArray.get("criteriaPoints").toString();
				
				if (criteriaName != null && !criteriaName.isEmpty() && value != null && !value.isEmpty() && criteria != null && !criteria.isEmpty()) {
					String[] splitCriteria = criteriaName.split(":");
					int headerId = Integer.parseInt(splitCriteria[0]);
					savePointsPartII += "(" +userId+ ", "+ i +"," +headerId+ ", '" +criteria+ "', '" +value+ "', '" +points+ "'),";
					insertTrue=true;
					
				}
				
				if(i== (size-1)) {
					savePointsPartII = savePointsPartII.substring(0, savePointsPartII.length() - 1);
				}
			}
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		System.out.println(savePointsPartI + savePointsPartII + savePointsCondition);
		String pointValues = "";
		
		if(insertTrue){
			databaseConnection.getConnected();
			pointValues = databaseConnection.insertPoints(savePointsPartI + savePointsPartII + savePointsCondition);
		}else {
			pointValues = "Please fill form before saving search.";
			System.out.println("Please fill form before saving search.");
		}
		String json = new ObjectMapper().writeValueAsString(pointValues);
	    return new ResponseEntity<String>(json, HttpStatus.OK);
	}
	
	@GetMapping("/getPointsConfiguration")
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	public ResponseEntity<String> getPointsConfiguration(HttpServletRequest request) throws JsonProcessingException, ClassNotFoundException 
	{
		String userId = request.getParameter("userId").toString();
		
		String pointSelect = "SELECT CONCAT(gis.parcel_metadata.header_id, \":\", gis.parcel_metadata.header_name) AS criteriaName, gis.point_configuration.point_condition, gis.point_configuration.point_value, gis.point_configuration.points ";
		String pointTables = "FROM gis.point_configuration ";
		String pointJoin = "INNER JOIN gis.parcel_metadata on gis.parcel_metadata.header_id = gis.point_configuration.header_id ";
		String pointWhere = "WHERE gis.point_configuration.user_id = " + userId + "";					
		
		String pointStatement = pointSelect + pointTables + pointJoin + pointWhere ;
        System.out.println(pointStatement);
	    databaseConnection.getConnected();
		ArrayList<Map<String, String>> pointsResult = databaseConnection.getPoints(pointStatement);
		String pointsResultString = new ObjectMapper().writeValueAsString(pointsResult);
        
		return new ResponseEntity<String>(pointsResultString, HttpStatus.OK);
	}
	
}
